#!/usr/bin/env python3

# Sloth  Copyright (C) 2015  https://bitbucket.org/pride
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import datetime
import os
import sys

# The directory this program is running from
main_dir = os.path.dirname(os.path.realpath(__file__))

# Saved exercises
log = os.path.join(main_dir, 'log.ini')


class Cardio():

    def __init__(self):
        self.hours = None
        self.minutes = None
        self.seconds = None
        self.distance = None

    def incorrect_format(self):
        print('Format only allows the following: 10:00 or 10:00:00')
        print('Meaning: digits and ":" only.')

    def over_999(self):
        print('You can only put up to 999 seconds/minutes at a time.')

    def playing_with_time(self, time1):
        sec_minutes, self.seconds = divmod(int(time1[-1]), 60)
        self.hours, self.minutes = divmod(int(time1[-2]), 60)
        self.minutes = sec_minutes + self.minutes
        if len(time1) == 3:
            self.hours += int(time1[0])
        elif self.minutes >= 60:
            self.hours, self.minutes = divmod(self.minutes, 60)

    def average_hours(self):
        total_seconds = (int(self.hours) * 3600 +
            int(self.minutes) * 60 +
            int(self.seconds))
        self.average_ = divmod(total_seconds / self.distance, 60)
        final_hours, final_minutes = divmod(self.average_[0], 60)
        self.aver = '{0:02.0f}:{1:02.0f}:{2:02.0f}'.format(
            int(final_hours),
            int(final_minutes),
            self.average_[1])

    def average_time(self, time1):
        try:
            if time1[2]:
                self.average_hours(self.distance)
        except IndexError:
            total_seconds = (int(self.minutes) * 60 +
            int(self.seconds))
            self.average_ = divmod(total_seconds / self.distance, 60)
            if self.hours:
                self.average_hours(self.distance)
            elif int(self.average_[0]) >= 60:
                self.average_hours(self.distance)
            else:
                self.aver = '{0:02.0f}:{1:02.0f}'.format(
                    int(self.average_[0]), int(self.average_[1]))

    def edited_hours(self): 
        print('Edited time is: {0:02d}:{1:02d}:{2:02d}'.format(
            int(self.hours),
            int(self.minutes),
            int(self.seconds)))

cardio_ = Cardio()

kind_check = False
while kind_check == False:
    try:
        what_kind = int(input('Running, Jogging, or Walking? (1/2/3): '))
        if what_kind > 3:
            print('The only options are 1/2/3')
        elif what_kind == 1:
            kind = 'run'
        elif what_kind == 2:
            kind = 'jog'
        elif what_kind == 3:
            kind = 'walk'
        if what_kind in [1, 2, 3]:
            kind_check = True
    except ValueError:
        print('The only options are 1/2/3')

time_check = False
while time_check == False:
    time = input('How long did you {0}?\n(Format is 10:00/10:00:00): '.format(
        kind))
    if '-' in time:
        print('Negative numbers are not allowed. Nice try though.')

    time1 = time.split(':')

    try:
    # People being stupid.
        if len(time1) == 1:
            cardio_.incorrect_format()

        elif len(time1) == 2:
            if int(time1[1]) > 999 or int(time1[0]) > 999:
                cardio_.over_999()
            else:
                if int(time1[0]) >= 60 or int(time1[1]) >= 60:
                    cardio_.playing_with_time(time1)
                    cardio_.edited_hours()
                else:
                    cardio_.playing_with_time(time1)
                    if len(time1) == 3:
                        cardio_.edited_hours()
                    else:
                        if (cardio_.minutes <= 9
                        or cardio_.seconds <= 9):
                            print('Edited time is: {0:02d}:{1:02d}'.format(
                            int(cardio_.minutes),
                            int(cardio_.seconds)))
                        else:
                            cardio_.minutes, cardio_.seconds = time1
                time_check = True

        elif len(time1) == 3:
            if int(time1[2]) > 999 or int(time1[1]) > 999:
                cardio_.over_999()
            elif int(time1[1]) >= 60 or int(time1[2]) >= 60:
                cardio_.playing_with_time(time1)
                cardio_.edited_hours()
            else:
                cardio_.playing_with_time(time1)
                if (cardio_.hours <= 9
                or cardio_.minutes <= 9
                or cardio_.seconds <= 9):
                    cardio_.edited_hours()
                else:
                    cardio_.minutes, cardio_.seconds = time1
                    
            time_check = True

            if cardio_.hours >= 24:
                print("Pretty sure you didn't {0} that long.".format(kind))

        else:
            cardio_.incorrect_format()
    except ValueError:
        cardio_.incorrect_format()

distance_check = False
while distance_check == False:
    try:
        cardio_.distance = float(input('How many miles did you go?: '))
    except (ValueError):
        print('A whole ( 1 ) or float ( 1.0 ) number is required')

    if cardio_.distance >= 50.0:
        print("Pretty sure you didn't run that far.")
    else: 
        distance_check = True

today = datetime.date.today()
cardio_.average_time(time1)

print ('Your average time was {0}'.format(cardio_.aver))

if cardio_.hours:
    log_time = '{0}:{1}:{2}'.format(
        int(cardio_.hours), int(cardio_.minutes), int(cardio_.seconds))
    with open(log, "a") as log1:
        log1.write(str(today.strftime("%B %d, %Y - /{0}/{1}/{2}/{3}/".format(
            kind.upper(),
            log_time,
            cardio_.distance,
            str(cardio_.aver).lstrip('0')))))
else:
    log_time = '{0}:{1}'.format(
        int(cardio_.minutes), int(cardio_.seconds))
    with open(log, "a") as log1:
        log1.write(str(today.strftime("%B %d, %Y - /{0}/{1}/{2}/{3}/".format(
            kind.upper(),
            log_time,
            cardio_.distance,
            str(cardio_.aver).lstrip('0')))))

kind_check = time_check = distance_check = False

# this is the concept for the running multipliers.
# using seconds as 2.5% to 10% and the minutes 60% to 130%.
# this'll probably get changed later to be much more fine-tuned
# but it's definitely a step in the right direction.
#
# base multipliers:
# running = 400
# jogging = 200
# walking = 100
#
# seconds:
# 00-14 = .1
# 15-29 = 0.075
# 30-44 = 0.05
# 45-59 = 0.025
#
# minutes:
# 11 = 0.6
# 10 = 0.7
# 9 = 0.8
# 8 = 0.9
# 7 = 1.0
# 6 = 1.1
# 5 = 1.2
# 4 = 1.3