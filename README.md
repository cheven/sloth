### Sloth Fitness RPG

### FAQ
- What operating systems are supported?
    - Debian 8 and Arch Linux have been tested.
    - Pretty much everything but Windows should work, but no guarantees.
- Python 2 or 3?
    - 3, and it's also at the top of most of the .py files.
- How do I run it?
    - Just run start.py and away you go!

----