#!/usr/bin/env python3

# Sloth  Copyright (C) 2015  https://bitbucket.org/pride
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

def bmi(settings_options, base, bmi_x):

    if settings_options[7] == '1':
        if bmi_x == 1:
            x = 0.75
        elif bmi_x == 2:
            x = 0.85
        elif bmi_x == 3:
            x = 0.95
        elif bmi_x == 4:
            x = 1.1

    elif settings_options[7] == '2':
        if bmi_x == 1:
            x = 0.75
        elif bmi_x == 2:
            x = 0.95
        elif bmi_x == 3:
            x = 1.1
        elif bmi_x == 4:
            x = 0.85

    elif settings_options[7] == '3':
        if bmi_x == 1:
            x = 0.95
        elif bmi_x == 2:
            x = 1.1
        elif bmi_x == 3:
            x = 0.85
        elif bmi_x == 4:
            x = 0.75

    # Round to the nearest whole number.
    print (round(base * x))