#!/usr/bin/env python3

# Sloth  Copyright (C) 2015  https://bitbucket.org/pride
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import datetime
import sys
import os
import defs

# The directory this program is running from
main_dir = os.path.dirname(os.path.realpath(__file__))

# File where all info on user is stored
settings = os.path.join(main_dir, 'settings.ini')

if sys.platform.startswith('linux') or sys.platform.startswith('darwin'):
    if os.geteuid() == 0:
        print ('DO. NOT. RUN. AS. ROOT.')
        sys.exit()
elif sys.platform.startswith('cygwin') or sys.platform.startswith('win'):
    import ctypes
    if ctypes.windll.shell32.IsUserAnAdmin() == 1:
        print ('DO. NOT. RUN. AS. ADMINISTRATOR.')
        sys.exit()

#Check if settings file exists with all information.
#If it does, start the main program.
#If not, work the defs magic.
if os.path.isfile(settings) and os.path.getsize(settings) > 0:
    with open(settings, 'r') as save1:
        first_line = save1.readline()
        settings_options = first_line.strip('/\n').split('/')
        if len(save1.readlines()) != 0:
            defs.initial()
    try:
        if len(settings_options) == 7:
            if int(settings_options[5]) in [1, 2, 3, 4]:
                date_full = settings_options[1]
                date_check = date_full.split('-')
            else:
                defs.initial()
            datetime_passed = False
            if int(len(date_check[2])) == 4:
                if int(len(date_check[1])) == 2:
                    if int(len(date_check[0])) == 2:
                        datetime.datetime.strptime(date_full, "%m-%d-%Y").date()
                        datetime_passed = True
            elif int(len(date_check[0])) == 4:
                if int(len(date_check[1])) == 2:
                    if int(len(date_check[2])) == 2:
                        datetime.datetime.strptime(date_full, "%Y-%m-%d").date()
                        datetime_passed = True
            else:
                defs.initial()
        else:
            defs.initial()

        if datetime_passed == False:
            defs.initial()

        all_done = True
        if all_done == True:
            all_done = False
            defs.hello(settings_options)
        else:
            defs.initial()
    except (ValueError, IndexError):
        all_done = False
        defs.initial()
else:
    defs.initial()
