#!/usr/bin/env python3

# Sloth  Copyright (C) 2015  https://bitbucket.org/pride
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Autocomplete taken from
# https://stackoverflow.com/questions/7821661/how-to-code-autocompletion-in-python
#
import datetime
import os
import readline
import sys
import multiplier
from workouts import workouts

# The directory this bot is running from
main_dir = os.path.dirname(os.path.realpath(__file__))

# File where all info on user is stored
settings = os.path.join(main_dir, 'settings.ini')

# Saved exercises
log = os.path.join(main_dir, 'log.ini')

if os.path.isfile(log):
    if os.path.getsize(log) <= 0:
        os.truncate(log, 0)
else:
    os.truncate(settings, 0)


def initial():

    os.truncate(settings, 0)

    name_check = False
    while name_check == False:
        name = input('Enter your first name (20 character limit): ')
        if len(name) > 20:
            print('There\'s a 20 character limit..')
        else:
            name_check = True

    age_check = False
    while age_check == False:
        try:
            age = input('Enter your birthday (like 12-31-1999, or 1999-12-31): ')
            date_ = age.split('-')
            if len(date_) >= 4:
                pass
            elif (int(len(date_[2])) == 4 and
                int(len(date_[1])) == 2 and
                int(len(date_[0])) == 2):
                    datetime.datetime.strptime(age, "%m-%d-%Y").date()
                    age_check = True
            elif (int(len(date_[0])) == 4 and
                int(len(date_[1])) == 2 and
                int(len(date_[2])) == 2):
                    datetime.datetime.strptime(age, "%Y-%m-%d").date()
                    age_check = True
            else:
                print('Format is 12-31-1999 or 1999-12-31')
        except (ValueError, IndexError):
            print('Format is 12-31-1999 or 1999-12-31')

    sex_check = False
    while sex_check == False:
        sex = input('Enter your sex (M/F): ')
        if sex.upper() != 'M' and sex.upper() != 'F':
            print('You didn\'t choose male or female.')
        else:
            sex_check = True

    weight_check = False
    while weight_check == False:
        try:
            weight = int(input('Enter weight in pounds (Whole numbers only): '))
            if weight == 0:
                print('You don\'t weigh anything?')
            elif weight >= 400:
                print('I seriously doubt you\'re that big.')
            elif weight < 400:
                weight_check = True
        except ValueError:
            print('Whole numbers only.')

    height_check = False
    while height_check == False:
        try:
            height = int(input('Enter height in inches: '))
            if height <= 20:
                print('Put in your real height, please.')
            if height >= 21 and height < 107:
                height_check = True
        except ValueError:
            print('Numbers (like 1.0 or 1) only.')

    goal_check = False
    while goal_check == False:
        try:
            print('What is your fitness goal?\n1 for power lifting')
            print('2 for strength\n3 for weight loss')
            goal = int(input('4 for cardio: '))
            if goal in [1, 2, 3, 4]:
                goal_check = True
            else:
                print('Options are: 1, 2, 3 or 4')
        except ValueError:
            print('Whole numbers only.')

    name_check = age_check = sex_check = False
    weight_check = height_check = goal_check = False

    with open(settings, 'a') as settings_:
        settings_.write('{0}/{1}/{2}/{3}/{4}/{5}'.format(
            name.capitalize(),
            age,
            sex.upper(),
            weight,
            height,
            goal))

    with open(settings, 'r') as save1:
        first_line = save1.readline()
        settings_options = first_line.strip('/\n').split('/')


def body_mass_index(weight, height):

    bmi = round((weight / (height * height)) * 703.0, 2)
    if bmi <= 18.5:
        bmi_word = 'underweight'
    elif bmi >= 18.6 and bmi <= 24.9:
        bmi_word = 'healthy'
    elif bmi >= 25 and bmi <= 29.9:
        bmi_word = 'overweight'
    else:
        bmi_word = 'obese'


class MyCompleter(object):  # Custom completer

    def __init__(self, options):
        self.options = sorted(options)

    def complete(self, text, state):
        if state == 0:  # on first trigger, build possible matches
            if text:  # cache matches (entries that start with entered text)
                self.matches = [s for s in self.options 
                                    if s and s.startswith(text.capitalize())]
            else:  # no text entered, all matches possible
                self.matches = self.options[:]

        # return match indexed by state
        try: 
            return self.matches[state]
        except IndexError:
            return None


def hello(settings_options):
#    your_height = '''{0:.0f}'{1:.0f}"'''.format(
#        *divmod(float(settings_options[2]), 12))

    height_format = '''{0:.0f}'{1:.0f}"'''.format(
        *divmod(int(settings_options[4]), 12))

    if settings_options[5] == '1':
        goal_string = 'power lifter'
    elif settings_options[5] == '2':
        goal_string = 'become stronger'
    elif settings_options[5] == '3':
        goal_string = 'lose weight'
    elif settings_options[5] == '4':
        goal_string = 'cardio'

    bday_ = settings_options[1].split('-')
    today = datetime.date.today()
    # [2] is year, [0] is month, [1] is day.
    if len(bday_[2]) == 4:
        current_age = today.year - int(
            bday_[2]) - ((today.month, today.day) < (int(bday_[0]), int(bday_[1])))
    # [0] is year, [1] is month, [2] is day.
    else:
        current_age = today.year - int(
            bday_[0]) - ((today.month, today.day) < (int(bday_[1]), int(bday_[2])))

    print('{0}/{1}/{2}'.format(
        settings_options[0],
        current_age,
        settings_options[2]))
    print('XP: {0}'.format(settings_options[6]))

    completer = MyCompleter([str(k) for k in workouts])
    readline.set_completer(completer.complete)
    readline.parse_and_bind('tab: complete')

    choose_check = False
    while choose_check == False:
        choose_ = input("What workout did you do? ")
        if choose_.capitalize() not in workouts.keys():
            choose_check = False
        else:
            if choose_.capitalize() == 'Cardio':
                import cardio
            else:
                time_ = int(input("For how many minutes? "))
                print(choose_)
                print(time_)
        choose_check = True

    #multiplier.bmi(settings_options, base, bmi_x)